package com.example.outla.recieptsgatherer2;

import android.app.DialogFragment;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class DisplayPreviousReceipts extends DialogFragment {

    public static final String TAG = DisplayPreviousReceipts.class.getSimpleName();

    public static DisplayPreviousReceipts newInstance() {

        return new DisplayPreviousReceipts();
    }
    public DisplayPreviousReceipts() {
        //empty constructor
    }

    DatabaseHelper mDatabaseHelper;
    private ListView mlistOfReceipts;
    private Listener mListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.display_receipts, container, false);
        mDatabaseHelper = new DatabaseHelper(getContext());
        ListView listView = view.findViewById(R.id.listOfReceipts);
        listView.deferNotifyDataSetChanged();
        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                populateListView());

        listView.setAdapter(listViewAdapter);
        populateListView();
        return view;
    }

    private ArrayList populateListView() {
        Log.d(TAG, "populateListView: Displaying data in the ListView.");
        //get the data and append to a list
        Cursor data = mDatabaseHelper.getData();
        ArrayList<String> listData = new ArrayList<>();
        while (data.moveToNext()) {
            //get the value from the database in column 1
            //then add it to the ArrayList
            listData.add(data.getString(1));

        }
        return listData;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (MainActivity)context;
        mListener.onDialogDisplayed();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.onDialogDismissed();
    }
}


