package com.example.outla.recieptsgatherer2;


import android.util.Log;

import org.json.JSONObject;


public class JsonConverter {

    public static JSONObject convertJson (String unFormatedJson) {

        try {
            JSONObject niceFormattedString = new JSONObject(unFormatedJson);

            return niceFormattedString;
        }catch (Exception e){
            Log.e("my app","the json cannot be converted");
            return null;
        }
    }
}
