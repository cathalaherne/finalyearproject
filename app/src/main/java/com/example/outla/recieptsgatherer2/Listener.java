package com.example.outla.recieptsgatherer2;

public interface Listener {

    void onDialogDisplayed();

    void onDialogDismissed();
}
